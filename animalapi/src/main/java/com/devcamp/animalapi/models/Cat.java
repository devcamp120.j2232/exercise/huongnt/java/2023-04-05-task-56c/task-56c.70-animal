package com.devcamp.animalapi.models;

public class Cat extends Mammal{

    public Cat(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Cat [" + super.toString() + "]";
    }

    
    
}
