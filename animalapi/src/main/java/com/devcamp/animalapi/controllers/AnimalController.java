package com.devcamp.animalapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animalapi.models.Cat;
import com.devcamp.animalapi.models.Dog;
import com.devcamp.animalapi.service.AnimalService;

@RestController
@CrossOrigin
@RequestMapping("/api")


public class AnimalController {
    @Autowired
    AnimalService animalService;
    
    @GetMapping("/cats")
    public ArrayList<Cat> getAllCatsApi(){
        return animalService.getAllCats();
    }


    @GetMapping("/dogs")
    public ArrayList<Dog> getAllDogsApi(){
        return animalService.getAllDogs();
    }



}
